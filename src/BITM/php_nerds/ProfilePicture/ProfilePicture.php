<?php


namespace App\ProfilePicture;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $picture;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("picture",$postData)){
            $this->picture = $postData['picture'];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->name,$this->picture) ;
        $sql = "insert into picture(name, picture) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);

        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }

        else{

            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;
        }

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "SELECT * FROM picture WHERE soft_deleted='No'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM picture WHERE id=".$this->id;
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();
    }

    public function update(){

        $dataArray = array($this->name,$this->picture);
        $sql = "UPDATE  picture SET name=?,picture=? where id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);

        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }

        else
        {

            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;
        }

        Utility::redirect('index.php');
    }

    public function trashed(){

        $sql = "SELECT * FROM picture WHERE soft_deleted='Yes'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function soft_delete(){

        $data_array = array("yes");
        $sql = "UPDATE picture SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been soft deleted!<br>");
            Utility::redirect('trashed.php');
        }
        else
        {

            Message::message("Failed! :( Data Has Not Been soft deleted!<br>");
            Utility::redirect('index.php');
        }
    }

    public function recover(){

        $data_array = array("No");
        $sql = "UPDATE picture SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been  recovered!<br>");

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  recovered!<br>");

        }

        Utility::redirect('trashed.php');
    }

    public function delete(){

        $sql = "DELETE FROM picture WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been  deleted!<br>")  ;

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;

        }

        Utility::redirect('index.php');
    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from picture  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from picture  WHERE soft_deleted = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from picture  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from picture  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }






    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  picture SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  picture SET soft_deleted='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from picture  WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from picture  WHERE id=".$id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['by_name']) && isset($requestArray['by_picture']) )  $sql = "SELECT * FROM `picture` WHERE `soft_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `picture` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['by_name']) && !isset($requestArray['by_picture']) ) $sql = "SELECT * FROM `picture` WHERE `soft_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['by_name']) && isset($requestArray['by_picture']) )  $sql = "SELECT * FROM `picture` WHERE `soft_deleted` ='No' AND `picture` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->picture);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->picture);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords
}