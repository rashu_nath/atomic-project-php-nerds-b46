<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 05.46
 */

require_once ("../../../vendor/autoload.php");

$objectProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objectProfilePicture->setData($_GET);

$objectProfilePicture->recover();