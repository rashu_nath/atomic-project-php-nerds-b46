<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 05.59
 */

require_once ("../../../vendor/autoload.php");

$objectProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objectProfilePicture->setData($_GET);

$one_data = $objectProfilePicture->view();

if(isset($_GET['yes']) && $_GET['yes']== 1){
    $objectProfilePicture->delete();
    $_GET['yes'] = 0;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title> delete </title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">


</head>

<body>

<div class="container">

  <h1 style="text-align: center">Do you really want to delete following record?</h1>
    <table class="table-bordered table-striped" border="1px">
        <tr>
            <th style="text-align: center; width:10%"> ID </th>
            <th style="text-align: center"> Name </th>
            <th style="text-align: center"> ProfilePicture </th>

        </tr>
        <tr>
            <?php echo "
                        <td style='text-align: center; width: 10%;'> $one_data->id</td>
                        <td style='text-align: center;'> $one_data->name</td>
                        <td style='text-align: center;'><img src='Upload/$one_data->picture' width='300px' height='100px'></td>
                            "; ?>

        </tr>
    </table>
    <a class="btn btn-danger" href='delete.php?id=<?php echo $one_data->id ?>&yes=1'>Yes</a>
    <a class="btn btn-info" href='trashed.php'>No</a>

 </div>
</body>
</html>
