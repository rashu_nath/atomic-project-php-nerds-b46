<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 03.04
 */


require_once ("../../../vendor/autoload.php");
$objectProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objectProfilePicture->setData($_GET);
$one_data = $objectProfilePicture->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Single View</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">

    <style>
        table{
            border: 1px;
        }
        td{
            border:0px;
        }
        tr{
            height: 30px;
        }
    </style>


</head>
<body>
<div class="container">
    <div class="nav">
        <td> <a class="btn btn-group-lg btn-info" href="index.php"> Active List </a> </td>
    </div>
    <table class="table-bordered table-striped" cellspacing="0px">
        <tr>
            <th  style="text-align: center; width:10%"> ID </th>
            <th style="text-align: center"> Name </th>
        </tr>
        <tr>
            <?php echo "
                        <td style='text-align: center; width: 10%;'> $one_data->id</td>
                        <td style='text-align: center;'> $one_data->name</td>
                        "; ?>

        </tr>
    </table>
    <div  class="img-responsive" style="width: 100%; height: 100%">
        <caption>
            <?php echo $one_data->picture ?>
        </caption>
        <img src="<?php echo "Upload/$one_data->picture" ?>" alt ='<?php echo $one_data->picture ?>' class="img-responsive">
    </div>
</div>
</body>
</html>
