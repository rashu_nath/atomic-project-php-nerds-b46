<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 05.06
 */

require_once ("../../../vendor/autoload.php");

$objectProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objectProfilePicture->setData($_GET);

$objectProfilePicture->soft_delete();