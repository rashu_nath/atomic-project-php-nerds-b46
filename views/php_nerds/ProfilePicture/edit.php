

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";


$objProfilePicture= new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile picture edit form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>



    <form  class="form-group " action="update.php" method="post" enctype="multipart/form-data">

        Enter Book Name:
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>">
        <br>
        Enter Author Name:
        <input class="form-control" type="file" name="picture">
        <br>

        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

        <input type="submit">

    </form>

    <img src="Upload/<?php echo $oneData->picture;?> ">

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


