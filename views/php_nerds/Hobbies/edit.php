<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";



$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();

$hobbies = explode(',',$oneData->hobbies);



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <form  class="form-group" action="update.php" method="post">

        Name:
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>">
        <br>
        Hobbies:
        <?php if(in_array("gardening", $hobbies)) echo "<input type='checkbox' name='hobbies[]' value='gardening' checked='checked'> gardening";
        else echo "<input type='checkbox' name='hobbies[]' value='gardening'> gardening";

        if(in_array("reading", $hobbies)) echo "<input type='checkbox' name='hobbies[]' value='reading' checked='checked'> reading";
        else echo "<input type='checkbox' name='hobbies[]' value='reading'> reading";
        if(in_array("facebooking", $hobbies)) echo "<input type='checkbox' name='hobbies[]' value='facebooking' checked='checked'> facebooking";
        else echo "<input type='checkbox' name='hobbies[]' value='facebooking'> facebooking";?>
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
        <input type="submit">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


