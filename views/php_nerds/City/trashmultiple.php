<?php
require_once("../../../vendor/autoload.php");

use \App\City\City;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$objCity= new City();


$objCity->trashMultiple($_POST['mark']);
    Utility::redirect("trashed.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("index.php");
}